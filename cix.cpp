// $Id: cix.cpp,v 1.2 2015-05-12 18:59:40-07 - - $

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unordered_map>
using namespace std;

#include <libgen.h>
#include <sys/types.h>
#include <unistd.h>

#include "protocol.h"
#include "logstream.h"
#include "sockets.h"

logstream log (cout);
struct cix_exit: public exception {};

unordered_map<string,cix_command> command_map {
   {"exit", CIX_EXIT},
   {"help", CIX_HELP},
   {"ls"  , CIX_LS  },
   {"rm"  , CIX_RM  },
   {"get" , CIX_GET },
   {"put" , CIX_PUT },
};

void cix_help() {
   static vector<string> help = {
      "exit         - Exit the program.  Equivalent to EOF.",
      "get filename - Copy remote file to local host.",
      "help         - Print help summary.",
      "ls           - List names of files on remote server.",
      "put filename - Copy local file to remote host.",
      "rm filename  - Remove file from remote server.",
   };
   for (const auto& line: help) cout << line << endl;
}

void cix_ls (client_socket& server) {
   cix_header header; 
   header.command = CIX_LS;
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
   recv_packet (server, &header, sizeof header);
   log << "received header " << header << endl;
   if (header.command != CIX_LSOUT) {
      log << "sent CIX_LS, server did not return CIX_LSOUT" << endl;
      log << "server returned " << header << endl;
   }else {
      char buffer[header.nbytes + 1];
      recv_packet (server, buffer, header.nbytes);
      log << "received " << header.nbytes << " bytes" << endl;
      buffer[header.nbytes] = '\0';
      cout << buffer;
   }
}

void cix_rm (client_socket& server, string filenombre) {
  cix_header header;
   header.command = CIX_RM;
   strcpy(header.filename, filenombre.c_str());
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
   recv_packet (server, &header, sizeof header);
   log << "received header " << header << endl;
   
   if (header.command != CIX_ACK) {
      log << "sent CIX_RM, server did not return CIX_ACK" << endl;
      log << "server returned " << header << endl;
   }
}

void cix_get (client_socket& server, string filenombre){

   cix_header header;
   header.command = CIX_GET;
   strcpy(header.filename, filenombre.c_str()); 
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
   recv_packet (server, &header, sizeof header);
   //recv_packet 
   log << "received header " << header << endl;
   
   if (header.command != CIX_FILE) {
      log << "sent CIX_GET, server did not return CIX_FILE" << endl;
      log << "server returned " << header << endl;
   }else {
        
      char buffer[header.nbytes + 1];
      recv_packet (server, buffer, header.nbytes);
      FILE * pFile = fopen (filenombre.c_str(), "w"); 
      log << "received " << header.nbytes << " bytes" << endl;
      buffer[header.nbytes] = '\0';
      //cout <<"hello "<<buffer;
      fputs(buffer, pFile); 
      fclose(pFile);
      }



}

void cix_put (client_socket& server, string filenombre){ 

cix_header header; 

FILE * pFile = fopen (filenombre.c_str(), "r");
  if (pFile == NULL) { 
      cout << "file does not exist: " << strerror (errno) << endl;
      header.command = CIX_NAK;
      header.nbytes = errno;
      return;
   }
    else{
            header.command = CIX_PUT;
        }
 fseek( pFile, 0, SEEK_END);

// get the file size
int Size = ftell(pFile);

//return the file pointer to begin of file if you want
rewind( pFile );



 
   string get_output;
   char buffer[0x1000];


   for (;;) {
      char* rc = fgets (buffer, sizeof buffer, pFile);
      if (rc == nullptr) break;
       get_output.append (buffer);
   }

   header.nbytes = Size;
   memset (header.filename, 0, FILENAME_SIZE);
   strcpy(header.filename, filenombre.c_str());
   log << "sending header " << header << endl;
   send_packet (server, &header, sizeof header);
    //cout<<"hello "<<get_output.c_str()<<"hi "<<Size<<endl;
   send_packet (server, get_output.c_str(), Size);
   log << "sent " << get_output.size() << " bytes" << endl;


}




void usage() {
   cerr << "Usage: " << log.execname() << " [host] [port]" << endl;
   throw cix_exit();
}

std::vector<std::string> &split(const std::string &s, char delim,
                                std::vector<std::string> &elems) {
    string delimstr = string(1, delim);

    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    if(s.size() == 0)
    {
        return elems;

    }

    if (s.substr(s.size() - 1, 1) == delimstr) {
        elems.push_back("");
        return elems;
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

int main (int argc, char** argv) {
   log.execname (basename (argv[0]));
   log << "starting" << endl;
   vector<string> args (&argv[1], &argv[argc]);
   if (args.size() > 2) usage();
 // string host = get_cix_server_host (args, 0);
   //in_port_t port = get_cix_server_port (args, 1);

// Defaults
in_port_t port = 65535;
string host = "127.0.0.1";

if (args.size() == 1) {
    if(is_number(args[0]) == true){
        port = get_cix_server_port (args, 0);
    }
    else{
        host = get_cix_server_host (args, 0);
    }  
} else if (args.size() == 2) {
    host = get_cix_server_host (args, 0);
    port = get_cix_server_port (args, 1);
}

   log << to_string (hostinfo()) << endl;
   try {
      log << "connecting to " << host << " port " << port << endl;
      client_socket server (host, port);
      log << "connected to " << to_string (server) << endl;
      for (;;) {
         string line;
         getline (cin, line);
        vector<string> command = split(line, ' ');
 

         if (cin.eof()) throw cix_exit();
         log << "command " << line << endl;
         const auto& itor = command_map.find (command[0]);
         cix_command cmd = itor == command_map.end()
                         ? CIX_ERROR : itor->second;
         switch (cmd) {
            case CIX_EXIT:
               throw cix_exit();
               break;
            case CIX_HELP:
               cix_help();
               break;
            case CIX_LS:
               cix_ls (server);
               break;
            case CIX_RM:
                cix_rm(server, command[1]);
               break;
            case CIX_GET:  
                if(command.size() < 2 || command[1] == ""){
                   cout<<" : invalid command"<<endl;
                }
                else{
                cix_get(server, command[1]);   
                }           
                break;
            case CIX_PUT:
                if(command.size() < 2 || command[1] == ""){
                  cout<<" : invalid command"<<endl;
                }
                else{
                cix_put(server, command[1]);   
                }           
                break;
            default:
               log << line << ": invalid command" << endl;
               break;
         }
      }
   }catch (socket_error& error) {
      log << error.what() << endl;
   }catch (cix_exit& error) {
      log << "caught cix_exit" << endl;
   }
   log << "finishing" << endl;
   return 0;
}

